filter

const arr = [1,2,3,4,5];
const odd = someArr => arr.filter(num => num%2 !== 0);
let filteredArr = odd(arr);

Filter is an array method which allows you to filter arrays by a certain condition. It loops through an array and returns values to the filtered array if the required condition is true. The function 'odd' filters an array to only include odd numbers. In the code above filteredArr would be [1,3,5];

------------------------------------------------------------------------------------------------------------

reduce

const arr = [1,2,3,4,5];
const theSum = arr.reduce((acc,curr) => acc + curr, 0);

Reduce is an array method which allows you to loop through an array. While looping through the array you have an accumulator and a current value as parameters. It is good for performing math operations(addition,subtraction,etc.), adding items to an array or object, and more. The code above finds the sum of arr (15).

------------------------------------------------------------------------------------------------------------

Lifecycle Hooks

1) componentWillMount

componentWillMount() {
    configuration setup, apis, etc.
}

Right before render function called. No DOM to manipulate yet. Will be deprecated in React 17.

------------------------------------------------------------------------------------------------------------

2) shouldComponentUpdate

shouldComponentUpdate(nextProps) {
    return nextProps.someProp !== this.props.someProp;
}

Tell whether component should re-render on state change. Check whether props have changed and return a boolean.

------------------------------------------------------------------------------------------------------------

3) componentDidUpdate

componentDidUpdate() {
    //network request, dom manipulation, etc.
}

Like componentDidMount, but doesn't fire when component initially renders.

------------------------------------------------------------------------------------------------------------

4) componentWillUnmount

componentWillUnmount() {
    clean up...remove event listeners, clean up network request, etc.
}

Called before component gets unmounted. Good for cleaning up network requests, removing event listeners, etc.