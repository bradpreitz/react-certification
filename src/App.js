import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
// #styling - css (not modules...not scoped to component)
import './App.css';
import Layout from './hoc/Layout/Layout';
import Home from './containers/Home/Home';
import Categories from './containers/Categories/Categories';
import Jokes from './containers/Jokes/Jokes';
import Viewed from './containers/Viewed/Viewed';
import Logout from './containers/Home/Logout/Logout';

export class App extends Component {
  render() {
    // #routing
    let routes = (
      <Switch>
        <Route path="/" exact component={Home} />
        <Redirect to="/" />
      </Switch>
    );

    if(this.props.authenticated) {
      routes = (
        <Switch>
          <Route path="/logout" component={Logout} />
          <Route path="/viewed" component={Viewed} />
          <Route path="/jokes" component={Jokes} />
          <Route path="/categories" component={Categories} />
          <Route path="/" exact component={Home} />
          <Redirect to="/" />
        </Switch>
      );
    }

    return (
      <Layout authenticated={this.props.authenticated}>
        {routes}
      </Layout>
    );
  } 
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.authenticated
  }
};

export default withRouter(connect(mapStateToProps)(App));
