import React from 'react';
import { shallow } from 'enzyme';

import ViewedJokesConnected, { Viewed } from './Viewed';;
import { storeFactory, findByTestAttr } from '../../test/testUtils';
import Modal from '../../components/UI/Modal/Modal';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<ViewedJokesConnected store={store}/>);
    return wrapper;
};

describe('<Viewed/>', () => {
    test('should render `no jokes found` if no jokes', () => {
        const wrapper = setup({
            viewedJokes: {
                viewedJokes: []
            }
        }).dive().dive();
        const noJokes = findByTestAttr(wrapper,'no-jokes-found');
        expect(noJokes.length).toBe(1);
    });

    test('should render viewed jokes if found', () => {
        const wrapper = setup({
            viewedJokes: {
                viewedJokes: ['test1','test2','test3']
            }
        }).dive().dive();
        const noJokes = findByTestAttr(wrapper,'viewed-joke');
        expect(noJokes.length).toBe(3);
    });

    test('should render modal on joke click', () => {
        const wrapper = setup({
            viewedJokes: {
                viewedJokes: ['test1']
            }
        }).dive().dive();
        const loginButton = findByTestAttr(wrapper,'viewed-joke');
        loginButton.simulate('click', {preventDefault() {}});
        expect(wrapper.containsMatchingElement(<Modal/>)).toBe(true);
    });
});