import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux/Aux';
import Classes from './Viewed.module.css';
import Modal from '../../components/UI/Modal/Modal';

export class Viewed extends Component {

    state = {
        showModal: false,
        currentJoke: null
    }

    showModal = (joke) => {
        this.setState({currentJoke:joke,showModal:true});
    };

    closeModal = () => {
        this.setState({showModal:false});
    };

    render() {
        let viewedJokes = null;
        if(this.props.viewedJokes) {
            if(this.props.viewedJokes.length){
                viewedJokes = (
                    <ul className={Classes.List}>
                        {this.props.viewedJokes.length ? this.props.viewedJokes.map((joke, index) => {
                            if(joke.length > 50) {
                                return (
                                    <li data-test="viewed-joke" onClick={() => this.showModal(joke)} key={'result-'+index}>{joke.slice(0,50)+'...'}</li>
                                )  
                            } else {
                                return (
                                    <li data-test="viewed-joke" onClick={() => this.showModal(joke)} key={'result-'+index}>{joke}</li>
                                )  
                            }
                        }) : <li>No Jokes Found</li>}
                    </ul>
                );
            } else {
                viewedJokes = (
                    <ul className={Classes.List}>
                        <li data-test="no-jokes-found">No Jokes Found</li>
                    </ul>            
                );            
            }
        }
        let modal = null;
        if (this.state.showModal) {
            modal = (
                <Modal closeModal={this.closeModal} show={this.state.showModal} joke={this.state.currentJoke}/>
            );
        }
        return (
            <Aux>
                <div className={Classes.ViewedJokes}>
                    <div className={Classes.Title}>
                        <p>Viewed Jokes</p>
                    </div>
                    <div className={Classes.Viewed}>
                        {viewedJokes}
                    </div>
                </div>
                {modal}
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        viewedJokes: state.viewedJokes.viewedJokes
    }
};

export default connect(mapStateToProps)(Viewed);