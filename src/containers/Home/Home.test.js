import React from 'react';
import { shallow } from 'enzyme';

import HomeConnected, { Home } from './Home';
import { storeFactory, findByTestAttr } from '../../test/testUtils';
import NavTiles from '../../components/Navigation/NavTiles/NavTiles';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<HomeConnected store={store}/>);
    return wrapper;
}

describe('<Home/>', () => {
    test('should render home page', () => {
        const wrapper = setup().dive().dive();
        const home = findByTestAttr(wrapper, 'home');
        expect(home.length).toBe(1);
    });

    test('should render login form if not authenticated', () => {
        const wrapper = setup({
            auth: {
                loading: false,
                authenticated: false,
                initialLogin: false
            }
        }).dive().dive();
        const loginForm = findByTestAttr(wrapper, 'login-form');
        expect(loginForm.length).toBe(1);
    });

    test('should render login navtiles if authenticated', () => {
        const wrapper = setup({
            auth: {
                loading: false,
                authenticated: true,
                initialLogin: false
            }
        }).dive().dive();
        expect(wrapper.containsMatchingElement(<NavTiles/>)).toBe(true);
    });

    test('should dispatch login action if valid form is submitted', () => {
        const loginMock = jest.fn();
        const props =  {
            email: {
                valid: true,
                touched: false
            },
            password: {
                valid: true,
                touched: false,
                show: false
            },
            login: loginMock
        };
        const wrapper = shallow(<Home {...props}/>);
        const loginButton = findByTestAttr(wrapper,'login-button');
        loginButton.simulate('click', {preventDefault() {}});
        const loginCount = loginMock.mock.calls.length;
        expect(loginCount).toBe(1);
    });
    
});