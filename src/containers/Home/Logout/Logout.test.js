import React from 'react';
import { shallow } from 'enzyme';
import { Redirect } from 'react-router-dom';
import { MemoryRouter } from 'react-router';

import LogoutConnected, { Logout } from './Logout';
import { storeFactory, findByTestAttr } from '../../../test/testUtils';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<MemoryRouter><LogoutConnected store={store}/></MemoryRouter>);
    return wrapper;
}

describe('<Logout/> redirect', () => {
    test('should render redirect to home page', () => {
        const wrapper = setup().dive().dive().dive().dive();
        expect(wrapper.containsMatchingElement(<Redirect to="/"/>)).toBe(true);
    });
});

describe('<Logout/> functions', () => {
    const clearJokesMock = jest.fn();
    const clearCategoriesMock = jest.fn();
    const logoutMock = jest.fn();
    
    const props = {
        clearJokes: clearJokesMock,
        clearCategories: clearCategoriesMock,
        logout: logoutMock
    };

    beforeEach(() => {
        const wrapper = shallow(<Logout {...props}/>);
        wrapper.instance().componentDidMount();
    });

    afterEach(() => {
        clearJokesMock.mockClear();
        clearCategoriesMock.mockClear();
        logoutMock.mockClear();
    });

    test('should call clearJokes on init', () => {
        const clearJokesCount = clearJokesMock.mock.calls.length;
        expect(clearJokesCount).toBe(1);
    });

    test('should call clearCategories on init', () => {
        const clearCategoriesCount = clearCategoriesMock.mock.calls.length;
        expect(clearCategoriesCount).toBe(1);
    });

    test('should call logout on init', () => {
        const logoutCount = logoutMock.mock.calls.length;
        expect(logoutCount).toBe(1);
    });
});