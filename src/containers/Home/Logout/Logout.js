import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import * as actions from '../../../store/actions/index';

export class Logout extends Component {
    componentDidMount() {
        this.props.clearJokes();
        this.props.clearCategories();
        this.props.logout();
    }

    render() {
        return <Redirect to="/"/>;
    }
}

const mapDispatchToProps = dispatch => {
    return {
        clearJokes: () => dispatch(actions.clearJokes()),
        clearCategories: () => dispatch(actions.clearCategories()),
        logout: () => dispatch(actions.authLogout())
    }
}

export default connect(null, mapDispatchToProps)(Logout);