import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import NavTiles from '../../components/Navigation/NavTiles/NavTiles';
import Classes from './Home.module.css';
import Spinner from '../../components/UI/Spinner/Spinner';
import * as actions from '../../store/actions/index';

export class Home extends Component {
    // #state
    state = {
        email: {
            valid: false,
            touched: false
        },
        password: {
            valid: false,
            touched: false,
            show: false
        }
    };

    hideShowPassword = () => {
        this.setState(prevState => {
            return {
                ...prevState,
                password: {
                    ...prevState.password,
                    show: !prevState.password.show
                }
            }
        });
    };

    validity = (inputType, event) => {
        let isValid = true;
        let val = event.target.value;
        if(val.trim() === 0) {
            isValid = false;
        }
        if(inputType === 'email') {
            const emailPattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = emailPattern.test(val);
        }else if(inputType === 'password') {
            const passwordPattern = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=^.{6,10}$)/;
            isValid = passwordPattern.test(val);
        }
        this.setState(prevState => {
            return {
                ...prevState,
                [inputType]: {
                    ...prevState[inputType],
                    valid: isValid
                }
            }
        });
    };

    setTouched = (inputType) => {
        if(!this.state[inputType].touched) {
            this.setState(prevState => {
                return {
                    ...prevState,
                    [inputType]: {
                        ...prevState[inputType],
                        touched: true
                    }
                }
            });
        }
    };

    formSubmit = (event) => {
        event.preventDefault();
    };

    render() {
        let homeContent = null;
        if(this.props.initialLogin) {
            homeContent = <Redirect to="/jokes"/>;
        }else if (this.props.loading) {
            homeContent = <Spinner/>;
        } else if(this.props.authenticated){
            homeContent = <NavTiles/>;
        } else {
            homeContent = (
                <div data-test="home" className={Classes.Home}>
                    <form data-test="login-form" className={Classes.HomeForm} onSubmit={this.formSubmit}>
                        <input
                            className={!this.state.email.valid && this.state.email.touched ? Classes.Invalid: null}
                            onChange={(event) => this.validity('email',event)}
                            onBlur={() => this.setTouched('email')}
                            type="email"
                            placeholder="email"
                        />
                        {!this.state.email.valid && this.state.email.touched ? <p className={Classes.InvalidText}>Please enter a valid email.</p> : null}
                        {/* #event handling */}
                        <input
                            className={!this.state.password.valid && this.state.password.touched ? Classes.Invalid: null}
                            onChange={(event) => this.validity('password',event)}
                            onBlur={() => this.setTouched('password')}
                            type={this.state.password.show ? 'text' : 'password'}
                            placeholder="password"
                        />
                        {!this.state.password.valid && this.state.password.touched ? <p className={Classes.InvalidText}>Password must contain at least 1 capital letter, 1 lower case letter, 1 number, and be between 6-10 characters in length.</p> : null}
                        <p className={Classes.ShowPassword} onClick={this.hideShowPassword}>show password</p>
                        <button
                        data-test="login-button"
                        className={!this.state.email.valid || !this.state.password.valid ? Classes.Disabled : null}
                        disabled={!this.state.email.valid || !this.state.password.valid}
                        onClick={this.props.login}>Login</button>
                    </form>
                </div>
            );
        }
        return (
            <div>
                {homeContent} 
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.authenticated,
        loading: state.auth.loading,
        initialLogin: state.auth.initialLogin
    }
};

const mapDispatchToProps = dispatch => {
    return {
        login: () => dispatch(actions.authLogin())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);