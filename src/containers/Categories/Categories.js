import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index';
import classes from './Categories.module.css';
import Modal from '../../components/UI/Modal/Modal';
import Spinner from '../../components/UI/Spinner/Spinner';

// #component
export class Categories extends Component {
    // #react lifecycle methods - componentDidMount
    componentDidMount() {
        if(!this.props.categories) {
            this.props.fetchCategories();
        }
    }
    // #closure
    randomJokeByCategory = (category) => {
        this.props.fetchRandomJoke(category);
    }

    // #arrow function
    closeModal = () => {
        // #this keyword
        this.props.fetchRandomJokeCloseModal();
    };
    // #react lifecycle methods - render
    render() {
        let categories = (
            <Spinner/>
        );
        // #react props
        if(!this.props.categoriesLoading && this.props.categories) {
            categories = (
                <ul className={classes.Categories}>
                    {/* #array functions */}
                    {/* #map function */}
                    {this.props.categories.map((category,index) => {
                        return (
                            // #closure
                            <li data-test="category" onClick={() => {this.randomJokeByCategory(category)}} key={category + '-' + index}>{category}</li>
                        )
                    })}
                </ul>
            );
        }
        let modal = null;
        if(this.props.showModal){
            if(this.props.randomJoke) {
                this.props.addViewedJoke(this.props.randomJoke);
            }
            modal = <Modal closeModal={this.closeModal} show={this.props.showModal} joke={this.props.randomJoke} loading={this.props.jokeLoading}/>;
        }
        return (
            <div>
                {categories}
                {modal}
            </div>
        );
    }
}

// #react props - mapping state to props
const mapStateToProps = state => {
    return {
        categoriesLoading: state.categories.categoriesLoading,
        categories: state.categories.categories,
        jokeLoading: state.categories.jokeLoading,
        randomJoke: state.categories.randomJoke,
        showModal: state.categories.showModal
    }
};
const mapDispatchToProps = dispatch => {
    return {
        fetchCategories: () => dispatch(actions.fetchCategories()),
        fetchRandomJoke: (category) => dispatch(actions.fetchRandomJoke(category)),
        fetchRandomJokeCloseModal: () => dispatch(actions.fetchRandomJokeCloseModal()),
        addViewedJoke: (viewedJoke) => dispatch(actions.addViewedJoke(viewedJoke))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories);