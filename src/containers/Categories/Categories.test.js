import React from 'react';
import { shallow } from 'enzyme';

import CategoriesConnected, { Categories } from './Categories';
import Spinner from '../../components/UI/Spinner/Spinner';
import { storeFactory, findByTestAttr } from '../../test/testUtils';
import Modal from '../../components/UI/Modal/Modal';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<CategoriesConnected store={store}/>);
    return wrapper;
};

describe('<Categories/>', () => {

    test('should render spinner if loading', () => {
        const wrapper = setup({
            categories: {
                categoriesLoading: true,
                categories: null,
                jokeLoading: false,
                randomJoke: null,
                showModal: false
            }
        }).dive().dive();
        expect(wrapper.containsMatchingElement(<Spinner/>)).toBe(true);
    });

    test('should render categories available', () => {
        const wrapper = setup({
            categories: {
                categoriesLoading: false,
                categories: ['test1','test2'],
                jokeLoading: false,
                randomJoke: null,
                showModal: false
            }
        }).dive().dive();
        const categories = findByTestAttr(wrapper, 'category');
        expect(categories.length).toBe(2);
    });

    
});

describe('<Categories/> functions', () => {
    const fetchCategoriesMock = jest.fn();
    const fetchRandomJokeMock = jest.fn();
    const addViewedJokeMock = jest.fn();

    const props = {
        fetchCategories: fetchCategoriesMock,
        fetchRandomJoke: null,
        fetchRandomJokeCloseModal: null,
        addViewedJoke: null,
        categoriesLoading: false,
        categories: null,
        jokeLoading: false,
        randomJoke: null,
        showModal: false
    };

    const propsLoaded = {
        fetchCategories: fetchCategoriesMock,
        fetchRandomJoke: fetchRandomJokeMock,
        fetchRandomJokeCloseModal: null,
        addViewedJoke: null,
        categoriesLoading: false,
        categories: ['test1'],
        jokeLoading: false,
        randomJoke: null,
        showModal: false
    };

    const propsRandomJoke = {
        fetchCategories: fetchCategoriesMock,
        fetchRandomJoke: fetchRandomJokeMock,
        fetchRandomJokeCloseModal: null,
        addViewedJoke: addViewedJokeMock,
        categoriesLoading: false,
        categories: ['test1'],
        jokeLoading: false,
        randomJoke: 'cool joke',
        showModal: true
    };

    afterEach(() => {
        fetchCategoriesMock.mockClear();
        fetchRandomJokeMock.mockClear();
        addViewedJokeMock.mockClear();
    });

    test('should fetch catgories on init if categories have not been set', () => {
        const wrapper = shallow(<Categories {...props}/>);
        wrapper.instance().componentDidMount();
        const fetchCategoriesCount = fetchCategoriesMock.mock.calls.length;
        expect(fetchCategoriesCount).toBe(1);
    });

    test('should not fetch catgories on init if categories have been set', () => {
        const wrapper = shallow(<Categories {...propsLoaded}/>);
        wrapper.instance().componentDidMount();
        const fetchCategoriesCount = fetchCategoriesMock.mock.calls.length;
        expect(fetchCategoriesCount).toBe(0);
    });
    
    test('should fetch random joke on category click', () => {
        const wrapper = shallow(<Categories {...propsLoaded}/>);
        const submit = findByTestAttr(wrapper, 'category');
        submit.simulate('click', {preventDefault() {} });
        const fetchJoke = fetchRandomJokeMock.mock.calls[0][0];
        expect(fetchJoke).toBe('test1');
    });

    test('should add viewed joke on random joke loaded', () => {
        const wrapper = shallow(<Categories {...propsRandomJoke}/>);
        const submit = findByTestAttr(wrapper, 'category');
        submit.simulate('click', {preventDefault() {} });
        const fetchJoke = addViewedJokeMock.mock.calls[0][0];
        expect(fetchJoke).toBe('cool joke');
    });

    test('should render modal on joke click', () => {
        const wrapper = shallow(<Categories {...propsRandomJoke}/>);
        const submit = findByTestAttr(wrapper, 'category');
        submit.simulate('click', {preventDefault() {} });
        expect(wrapper.containsMatchingElement(<Modal/>)).toBe(true);
    });
});