import React, { useState, useEffect, useReducer, useRef } from 'react';
import { connect } from 'react-redux';

import axios from '../../axios';
import Aux from '../../hoc/Aux/Aux';
import Spinner from '../../components/UI/Spinner/Spinner';
import Modal from '../../components/UI/Modal/Modal';
// #styling - css modules
import Classes from './Jokes.module.css';
import * as actions from '../../store/actions/index';

export const Jokes = props => {
    // #react hooks - useState
    const [currentJoke,setCurrentJoke] = useState({
        showModal: false,
        currentJoke: null
    });

    let searchInputRef = useRef();

    // #Blocked-scoped variables const
    const currentJokeReducer = (state, action) => {
        switch (action.type) {
            case 'LOADING':
                return action.payload;
            case 'LOADED':
                return action.payload;
            default:
                return state;
        }
    };

    // #destructuring
    // #react hooks - useReducer
    const [jokes, dispatch] = useReducer(currentJokeReducer, {jokesLoading: false, searchResults: null});

    // #react hooks - useEffect
    useEffect(() => {
        if(props.initialLogin){
            props.setAuthInitFalse();
        }
    }, []);


    const jokeSearch = (searchVal) => {
        dispatch({type: 'LOADING', payload: {jokesLoading: true, searchResults: null}});
        // #promises
        axios.get('/search?query='+searchVal)
            .then(response => {
                dispatch({type: 'LOADED', payload: {jokesLoading: false, searchResults: response.data.result}});
            })
            .catch(() => {
                dispatch({type: 'LOADED', payload: {jokesLoading: false, searchResults: []}});
            });
    }

    const showModal = (joke) => {
        setCurrentJoke({currentJoke: joke, showModal: true});
    };

    const closeModal = () => {
        setCurrentJoke({currentJoke: null, showModal: false});
    };

    const formSubmit = (event) => {
        event.preventDefault();
        jokeSearch(searchInputRef.current.value);
    };

    let searchResults = null;
    if(jokes.searchResults) {
        if(jokes.searchResults.length){
            searchResults = (
                <ul className={Classes.List}>
                    {jokes.searchResults.length ? jokes.searchResults.map((joke, index) => {
                        if(joke.value.length > 50) {
                            return (
                                // #lists & keys
                                <li data-test="joke-click" onClick={() => showModal(joke.value)} key={'result-'+index}>{joke.value.slice(0,50)+'...'}</li>
                            )  
                        } else {
                            return (
                                <li data-test="joke-click" onClick={() => showModal(joke.value)} key={'result-'+index}>{joke.value}</li>
                            )  
                        }
                    }) : <li>No Jokes Found</li>}
                </ul>
            );
        } else {
            searchResults = (
                <ul className={Classes.List}>
                    <li>No Jokes Found</li>
                </ul>            
            );            
        }
    }
    // #Blocked-scoped variables let
    let modal = null;
    if (currentJoke.showModal) {
        props.addViewedJoke(currentJoke.currentJoke);
        modal = (
            <Modal closeModal={closeModal} show={currentJoke.showModal} joke={currentJoke.currentJoke}/>
        );
    }
    // #JSX
    return (
        <Aux>
            {/* #styling - css modules */}
            <div data-test="jokes" className={Classes.Jokes}>
                <form data-test="jokes-form" 
                className={Classes.JokesForm}
                onSubmit={formSubmit}
                >
                    <input
                    ref={searchInputRef}
                        data-testid="searchInput"
                        data-test="jokes-input"
                        placeholder="Joke Search"
                        type="text"/>
                    <button 
                    data-test="submit-jokes-search"
                    data-testid="searchButton"
                    >Submit</button>
                </form>
                <div className={Classes.Title}>
                    <p>Search Results</p>
                </div>
                <div 
                data-testid="searchResults"
                className={Classes.Results}
                >
                    {jokes.jokesLoading ? <Spinner/> : null}
                    {searchResults}
                </div>
            </div>
            {modal}
        </Aux>
    );
}

const mapStateToProps = state => {
    return {
        initialLogin: state.auth.initialLogin
    }
};

const mapDispatchToProps = dispatch => {
    return {
        setAuthInitFalse: () => dispatch(actions.authInitFalse()),
        addViewedJoke: (viewedJoke) => dispatch(actions.addViewedJoke(viewedJoke))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Jokes);