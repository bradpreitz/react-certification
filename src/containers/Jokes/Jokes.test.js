import React, {useReducer} from 'react';
import { shallow} from 'enzyme';
import {render, fireEvent, cleanup,renderHook} from '@testing-library/react';
import {getByTestId} from '@testing-library/dom';
import JokesConnected, { Jokes } from './Jokes';
import { storeFactory, findByTestAttr } from '../../test/testUtils';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<JokesConnected store={store}/>);
    return wrapper;
};

describe('<Jokes/> basic setup', () => {
        const wrapper = setup().dive().dive();
    test('should render jokes component', () => {
        const jokes = findByTestAttr(wrapper, 'jokes');
        expect(jokes.length).toBe(1);
    });
    test('should render jokes search form', () => {
        const jokes = findByTestAttr(wrapper, 'jokes-form');
        expect(jokes.length).toBe(1);
    });
    test('should render jokes search form input', () => {
        const jokes = findByTestAttr(wrapper, 'jokes-input');
        expect(jokes.length).toBe(1);
    });
    test('should render jokes search form button', () => {
        const jokes = findByTestAttr(wrapper, 'submit-jokes-search');
        expect(jokes.length).toBe(1);
    });
    
});

describe('<Jokes/> deeper checks', () => {

    afterEach(cleanup);

    test('results are empty on', () => {
        const {container} = render(<Jokes />);
        const searchResults = getByTestId(container, "searchResults");
        expect(searchResults.textContent).toBe('');
    });
    
    test('spinner loads after joke search submit', () => {
        const {container} = render(<Jokes />);
        const jokeSearchInput = getByTestId(container, "searchInput");
        const searchResults = getByTestId(container, "searchResults");
        const button = getByTestId(container, "searchButton");
        jokeSearchInput.value = 'bradley';
        fireEvent.change(jokeSearchInput);
        fireEvent.click(button);
        expect(searchResults.textContent).toBe('Loading...');
    });

});