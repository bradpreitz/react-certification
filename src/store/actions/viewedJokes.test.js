import { storeFactory } from '../../test/testUtils';
import { addViewedJoke, clearJokes } from './viewedJokes';
import rootReducer from '../reducers/index';

describe('viewed joke actions', () => {
    test('should add viewed joke', () => {
        const store = storeFactory({...rootReducer});
        store.dispatch(addViewedJoke('Chuck Norris did in fact, build Rome in a day.'));
        const newState = store.getState().viewedJokes.viewedJokes[0];
        expect(newState).toBe('Chuck Norris did in fact, build Rome in a day.');
    });
    test('should clear viewed jokes', () => {
        const store = storeFactory({...rootReducer});
        store.dispatch(clearJokes());
        const newState = store.getState().viewedJokes.viewedJokes;
        expect(newState.length).toBe(0);
    });
});