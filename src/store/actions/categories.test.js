import { storeFactory } from '../../test/testUtils';
import { fetchCategories, fetchRandomJoke } from './categories';
import moxios from 'moxios';
import instance from '../../axios';


describe('categories actions', () => {
  let axiosInstance;
  beforeEach(() => {
    axiosInstance = instance;
    moxios.install(axiosInstance);
  });
  afterEach(() => {
    moxios.uninstall(axiosInstance);
  });

  test('fetchCategories action creator should work', function (done) {
    
    const store = storeFactory();

    store.dispatch(fetchCategories()).then(() => {
      const newState = store.getState();
      expect(newState.categories.categories.length).toBe(2);
      done();
    });

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: ['cat1','cat2']
      });
    });

  });

  test('fetchRandomJoke action creator should work', function (done) {
    
    const store = storeFactory();

    store.dispatch(fetchRandomJoke('animal')).then(() => {
      const newState = store.getState();
      expect(newState.categories.randomJoke).toBe('some funny joke');
      done();
    });

    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: 200,
        response: {
          value: 'some funny joke'
        }
      });
    });

  });

});