import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const fetchCategoriesSuccess = (categories) => {
    return {
        type: actionTypes.FETCH_CATEGORIES_SUCCESS,
        categories: categories
    };
};

export const fetchCategoriesFail = (errorMessage) => {
    return {
        type: actionTypes.FETCH_CATEGORIES_FAIL,
        errorMessage: errorMessage
    };
};

export const fetchCategoriesStart = () => {
    return {
        type: actionTypes.FETCH_CATEGORIES_START
    };
};

export const clearCategories = () => {
    return {
        type: actionTypes.CLEAR_CATEGORIES
    };
};

export const fetchCategories = () => {
    return dispatch => {
        dispatch(fetchCategoriesStart());
        return axios.get('/categories')
            .then(response => {
                dispatch(fetchCategoriesSuccess(response.data));
            })
            .catch(error => {
                dispatch(fetchCategoriesFail('Server Error Here'));
            });
    }
};

export const fetchRandomJokeSuccess = (randomJoke) => {
    return {
        type: actionTypes.FETCH_RANDOM_JOKE_SUCCESS,
        randomJoke: randomJoke
    };
};

export const fetchRandomJokeFail = (errorMessage) => {
    return {
        type: actionTypes.FETCH_RANDOM_JOKE_FAIL,
        errorMessage: errorMessage
    };
};

export const fetchRandomJokeStart = () => {
    return {
        type: actionTypes.FETCH_RANDOM_JOKE_START
    };
};

export const fetchRandomJokeCloseModal = () => {
    return {
        type: actionTypes.FETCH_RANDOM_JOKE_CLOSE_MODAL
    };
};

export const fetchRandomJoke = (category) => {
     return dispatch => {
        dispatch(fetchRandomJokeStart());
        return axios.get('/random?category='+category)
            .then(response => {
                dispatch(fetchRandomJokeSuccess(response.data.value));
            })
            .catch(error => {
                dispatch(fetchRandomJokeFail('Server Error Here'));
            });
    }
};