import * as actionTypes from './actionTypes';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    };
};

export const authInitFalse = () => {
    return {
        type: actionTypes.AUTH_INIT_FALSE
    };
};

export const authSuccess = () => {
    return {
        type: actionTypes.AUTH_SUCCESS
    };
};

export const authLogin = () => {
    return dispatch => {
        dispatch(authStart());
        setTimeout(() => {
            dispatch(authSuccess());
        },2000);
    }
};

export const authLogout = () => {
    return {
        type: actionTypes.AUTH_LOGOUT
    };
};