import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from './auth';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('auth actions', () => {
    let store;
    beforeEach(() => {
        store = mockStore({ auth: {authenticated: false} });
    });
    afterEach(() => {
        store.clearActions();
    });
    test('should dispatch authSuccess on login', () => {
        jest.useFakeTimers();
        const expectedActions = [
            {"type": "AUTH_START"},
            {"type": "AUTH_SUCCESS"}
        ];
        store.dispatch(actions.authLogin());
        jest.runAllTimers();
        expect(store.getActions()).toEqual(expectedActions);
    });
});
