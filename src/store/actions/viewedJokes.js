import * as actionTypes from './actionTypes';

export const addViewedJoke = (viewedJoke) => {
    return {
        type: actionTypes.ADD_VIEWED_JOKE,
        viewedJoke: viewedJoke
    };
};

export const clearJokes = () => {
    return {
        type: actionTypes.CLEAR_JOKES
    };
};