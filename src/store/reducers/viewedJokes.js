import * as actionTypes from '../actions/actionTypes';

const initialState = {
    viewedJokes: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.ADD_VIEWED_JOKE:
            return {
                ...state,
                viewedJokes: !state.viewedJokes.includes(action.viewedJoke) ? state.viewedJokes.concat(action.viewedJoke) : state.viewedJokes
            }
        case actionTypes.CLEAR_JOKES:
            return {
                ...state,
                viewedJokes: []
            }
        default:
            return state;
    }
};

export default reducer;