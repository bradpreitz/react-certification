import reducer from './viewedJokes';
import * as actionTypes from '../actions/actionTypes';

describe('viewedJokes reducer', () => {

    test('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            viewedJokes: []
        })
    });

    test('should add joke', () => {
        expect(reducer({
            viewedJokes: []
        }, {
            type: actionTypes.ADD_VIEWED_JOKE,
            viewedJoke: ['test joke']
        })).toEqual({
            viewedJokes: ['test joke']
        })
    });

    test('should clear jokes', () => {
        expect(reducer({
            viewedJokes: []
        }, {
            type: actionTypes.CLEAR_JOKES
        })).toEqual({
            viewedJokes: []
        })
    });

});