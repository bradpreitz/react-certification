import * as actionTypes from '../actions/actionTypes';

const initialState = {
    categoriesLoading: true,
    categories: null,
    jokeLoading: false,
    randomJoke: null,
    showModal: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.FETCH_CATEGORIES_START:
            return {
                ...state,
                categoriesLoading: true
            };
        case actionTypes.FETCH_CATEGORIES_SUCCESS:
            return {
                ...state,
                categoriesLoading: false,
                categories: action.categories
            };
        case actionTypes.FETCH_CATEGORIES_FAIL:
            return {
                ...state,
                categoriesLoading: false,
                categories: [action.errorMessage]
            };
        case actionTypes.CLEAR_CATEGORIES:
            return {
                ...state,
                categoriesLoading: true,
                categories: null
            };
        case actionTypes.FETCH_RANDOM_JOKE_START:
            return {
                ...state,
                jokeLoading: true,
                randomJoke: null,
                showModal: true
            };
        case actionTypes.FETCH_RANDOM_JOKE_SUCCESS:
            return {
                ...state,
                jokeLoading: false,
                randomJoke: action.randomJoke
            };
        case actionTypes.FETCH_RANDOM_JOKE_FAIL:
            return {
                ...state,
                jokeLoading: false,
                randomJoke: action.errorMessage
            };
        case actionTypes.FETCH_RANDOM_JOKE_CLOSE_MODAL:
            return {
                ...state,
                jokeLoading: false,
                showModal: false
            };
        default:
            return state;
    }
};

export default reducer;