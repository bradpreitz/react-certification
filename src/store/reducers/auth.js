import * as actionTypes from '../actions/actionTypes';

const initialState = {
    loading: false,
    authenticated: false,
    initialLogin: false
};
// #redux reducer
const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.AUTH_START:
            // #spread operator
            return {...state,loading:true};
        case actionTypes.AUTH_SUCCESS:
            return {...state,loading:false,authenticated:true,initialLogin:true};
        case actionTypes.AUTH_LOGOUT:
            return {...state,authenticated:false};
        case actionTypes.AUTH_INIT_FALSE:
            return {...state,initialLogin:false};
        default:
            return state;
    }
};

export default reducer;