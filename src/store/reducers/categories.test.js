import reducer from './categories';
import * as actionTypes from '../actions/actionTypes';

describe('categories reducer', () => {

    test('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        })
    });

    test('should add categories', () => {
        expect(reducer({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        }, {
            type: actionTypes.FETCH_CATEGORIES_SUCCESS,
            categories: ['test','test2'] 
        })).toEqual({
            categoriesLoading: false,
            categories: ['test','test2'],
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        })
    });

    test('should clear categories', () => {
        expect(reducer({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        }, {
            type: actionTypes.CLEAR_CATEGORIES
        })).toEqual({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        })
    });

    test('should fetch random joke', () => {
        expect(reducer({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: null,
            showModal: false
        }, {
            type: actionTypes.FETCH_RANDOM_JOKE_SUCCESS,
            randomJoke: 'random joke'
        })).toEqual({
            categoriesLoading: true,
            categories: null,
            jokeLoading: false,
            randomJoke: 'random joke',
            showModal: false
        })
    });

});