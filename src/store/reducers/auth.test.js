import reducer from './auth';
import * as actionTypes from '../actions/actionTypes';

describe('auth reducer', () => {

    test('should return initial state', () => {
        expect(reducer(undefined, {})).toEqual({
            loading: false,
            authenticated: false,
            initialLogin: false
        })
    });

    test('should login', () => {
        expect(reducer({
            loading: false,
            authenticated: false,
            initialLogin: false
        }, {
            type: actionTypes.AUTH_SUCCESS
        })).toEqual({
            loading: false,
            authenticated: true,
            initialLogin: true
        })
    });

    test('should logout', () => {
        expect(reducer({
            loading: false,
            authenticated: true,
            initialLogin: false
        }, {
            type: actionTypes.AUTH_LOGOUT
        })).toEqual({
            loading: false,
            authenticated: false,
            initialLogin: false
        })
    });

});