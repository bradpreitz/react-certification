import { combineReducers } from 'redux';
import authReducer from './auth';
import categoriesReducer from './categories';
import viewedJokesReducer from './viewedJokes';

export default combineReducers({
    auth: authReducer,
    categories: categoriesReducer,
    viewedJokes: viewedJokesReducer
});