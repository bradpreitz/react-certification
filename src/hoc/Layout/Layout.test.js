import React from 'react';
import { shallow } from 'enzyme';

import Layout from './Layout';
import { storeFactory } from '../../test/testUtils';
import Header from '../../components/Navigation/Header/Header';

const setup = (state={}) => {
    const store = storeFactory(state);
    const wrapper = shallow(<Layout store={store}/>);
    return wrapper;
}

describe('<Layout/>', () => {
    test('should render layout header', () => {
        const wrapper = setup({
            auth: {
                authenticated: true
            }
        }).dive().dive();
        expect(wrapper.containsMatchingElement(<Header/>)).toBe(true);
    });

    test('should render layout main', () => {
        const wrapper = setup({
            auth: {
                authenticated: true
            }
        }).dive().dive();
        expect(wrapper.containsMatchingElement(<main/>)).toBe(true);
    });
});