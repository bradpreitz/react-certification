import React, { Component } from 'react';
import { connect } from 'react-redux';

import Aux from '../Aux/Aux';
import Header from '../../components/Navigation/Header/Header';
import Classes from './Layout.module.css';

class Layout extends Component {
    render() {
        return (
            <Aux>
                <Header authenticated={this.props.authenticated} />
                <main className={Classes.Main}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        authenticated: state.auth.authenticated
    }
}

export default connect(mapStateToProps)(Layout);