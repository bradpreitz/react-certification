import React from 'react';
import { shallow } from 'enzyme';
import { MemoryRouter } from 'react-router';

import { storeFactory } from './test/testUtils';
import App from './App';

const setup = (state={}) => {
  const store = storeFactory(state);
  const wrapper = shallow(<MemoryRouter initialEntries={["/"]}><App store={store}/></MemoryRouter>).dive().dive().dive().dive().dive().dive().dive();
  return wrapper;
}

test('renders without error', () => {
  const wrapper = shallow(<App/>);
});

test('authenticated prop is true if logged in', () => {
  const wrapper = setup({auth: {loading: false,
    authenticated: true,
    initialLogin: false}});
  expect(wrapper.instance().props.authenticated).toBe(true);
});

test('authenticated prop is false if not logged in', () => {
  const wrapper = setup({auth: {loading: false,
    authenticated: false,
    initialLogin: false}});
  expect(wrapper.instance().props.authenticated).toBe(false);
});