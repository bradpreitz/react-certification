import React from 'react';

import { shallow } from 'enzyme';
import Modal from './Modal';
import { findByTestAttr } from '../../../test/testUtils';
import Spinner from '../Spinner/Spinner';

describe('<Modal/>', () => {
    const setup = (props={}) => {
        const setupProps = {...props};
        return shallow(<Modal {...setupProps}/>);
    };
    
    test('should render modal', () => {
        const wrapper = setup({});
        expect(wrapper.find('div.Modal')).toHaveLength(1);
    });

    test('should render spinner if no joke', () => {
        const wrapper = setup({joke:null});
        expect(wrapper.containsMatchingElement(<Spinner/>)).toBe(true);
    });

    test('should render joke', () => {
        const wrapper = setup({joke:'test'});
        const component = findByTestAttr(wrapper, 'modal-joke');
        expect(component.length).toBe(1);
    });
    
});