import React from 'react';

import Aux from '../../../hoc/Aux/Aux';
import Backdrop from '../Backdrop/Backdrop';
import Classes from './Modal.module.css';
import Spinner from '../../../components/UI/Spinner/Spinner';

const Modal = (props) => {
    return (
        <Aux>
            <Backdrop show={props.show} clicked={props.closeModal}/>
            <div className={Classes.Modal}>
                {props.joke ? <p data-test="modal-joke">{props.joke}</p> : <Spinner/>}
            </div>
        </Aux>
    );
};

export default Modal;