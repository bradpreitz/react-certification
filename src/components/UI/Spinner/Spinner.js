import React from 'react';

import Classes from './Spinner.module.css';

const Spinner = () => {
    return (
        <div data-testid="spinner" data-test="spinner" className={Classes.loader}>Loading...</div>
    );
};

export default Spinner;