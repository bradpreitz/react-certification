import React from 'react';

import { findByTestAttr } from '../../../test/testUtils';
import { shallow } from 'enzyme';
import Spinner from './Spinner';

describe('<Spinner/>', () => {

    test('should render spinner', () => {
        const wrapper = shallow(<Spinner />);
        const spinner = findByTestAttr(wrapper, 'spinner');
        expect(spinner.length).toBe(1);
    });

});