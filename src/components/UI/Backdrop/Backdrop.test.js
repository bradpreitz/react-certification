import React from 'react';

import { shallow } from 'enzyme';
import Backdrop from './Backdrop';

describe('<Backdrop/>', () => {
    const setup = (props={}) => {
        const setupProps = {...props};
        return shallow(<Backdrop {...setupProps}/>);
    }

    test('should not render a backdrop if show is false', () => {
        const wrapper = setup({show: false});
        expect(wrapper.find('div.Backdrop')).toHaveLength(0);
    });

    test('should render a backdrop if show is true', () => {
        const wrapper = setup({show: true});
        expect(wrapper.find('div.Backdrop')).toHaveLength(1);
    });
    
});