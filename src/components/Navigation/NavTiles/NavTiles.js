import React from 'react';
import { NavLink } from 'react-router-dom';

import Classes from './NavTiles.module.css';

const NavTiles = () => {
    return (
        <div className={Classes.NavTiles}>
            <NavLink className={[Classes.Tile,Classes.MargBottom].join(' ')} to='/categories'>
                <div className={Classes.Bg}>
                    <h2>Chuck Norris<br/>Joke Categories</h2>
                </div>
            </NavLink> 
            <NavLink className={[Classes.Tile,Classes.MargBottom].join(' ')} to='/jokes'>
                <div className={Classes.Bg}>
                    <h2>Chuck Norris<br/>Joke Search</h2>
                </div>
            </NavLink> 
            <NavLink className={Classes.Tile} to='/viewed'>
                <div className={Classes.Bg}>
                    <h2>Chuck Norris<br/>Viewed Jokes</h2>
                </div>
            </NavLink> 
        </div>
    );
};

export default NavTiles;