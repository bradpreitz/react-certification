import React from 'react';

import { shallow } from 'enzyme';
import NavTiles from './NavTiles';
import { NavLink } from 'react-router-dom';

describe('<NavTiles/>', () => {

    test('should render 3 nav links', () => {
        const wrapper = shallow(<NavTiles />);
        expect(wrapper.find(NavLink)).toHaveLength(3);
    });
    
});