import React from 'react';
import { NavLink } from 'react-router-dom';

import chuckNorris from '../../../assets/images/chuck_norris.svg';
import Classes from './Header.module.css';

const Header = (props) => {
    let links = null;
    if(props.authenticated){
        links = (
            <ul>
                <li>
                    <NavLink to='/categories' activeClassName={Classes.Active}>
                        Categories
                    </NavLink>
                </li> 
                <li>
                    <NavLink to='/jokes' activeClassName={Classes.Active}>
                        Jokes
                    </NavLink>
                </li> 
                <li>
                    <NavLink to='/viewed' activeClassName={Classes.Active}>
                        Viewed
                    </NavLink>
                </li> 
                <li>
                    <NavLink to='/logout' activeClassName={Classes.Active}>
                        Logout
                    </NavLink>
                </li> 
            </ul>
        );
    }
    return (
        <header>
            <div className="logo">
                <NavLink to='/'>
                    <img className={Classes.logo} src={chuckNorris} alt="chuck norris"/>
                </NavLink>
            </div>
            {links}
        </header>
    );
};

export default Header;