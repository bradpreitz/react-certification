import React from 'react';
import { shallow } from 'enzyme';

import Header from './Header';

describe('<Header/>', () => {
    const setup = (props={}) => {
        const setupProps = {...props};
        return shallow(<Header {...setupProps}/>);
    }
 
    test('should render 1 link if not authenticated', () => {
        const wrapper = setup({authenticated: false});
        expect(wrapper.find('NavLink')).toHaveLength(1);
    });

    test('should render 5 links if authenticated', () => {
        const wrapper = setup({authenticated: true});
        expect(wrapper.find('NavLink')).toHaveLength(5);
    });
    
});